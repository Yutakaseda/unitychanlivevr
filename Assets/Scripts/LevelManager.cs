﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	[SerializeField]
	GameObject[] liveObjects;

	enum LiveStatus{
		WAIT,
		LIVE
	}

	LiveStatus state;

	void Awake(){
		state = LiveStatus.WAIT;
	}

	public void LiveStart(){
		if (state == LiveStatus.WAIT)
			foreach (var obj in liveObjects)
				obj.SetActive (true);
		state = LiveStatus.LIVE;

	}
}
