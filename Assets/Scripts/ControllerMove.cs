﻿using UnityEngine;
using System.Collections;

public class ControllerMove : MonoBehaviour {

	[SerializeField]
	GameObject controllerPivot = null;
	[SerializeField]
	LevelManager level;
	[SerializeField]
	GameObject[] lighting;

	void Update () {
		Move();

		if (GvrController.ClickButtonDown) {
			foreach (var obj in lighting) {
				obj.SetActive (!obj.activeSelf);
			}
		}
		if (GvrController.AppButtonDown) {
			GvrViewer.Instance.Recenter ();
			level.LiveStart ();
		}
	}

	void Move(){
		if (GvrController.State != GvrConnectionState.Connected) {
			controllerPivot.SetActive(false);
		}
		controllerPivot.SetActive(true);
		controllerPivot.transform.rotation = GvrController.Orientation;
	}
}
